import React, { useState } from 'react'
import Mensaje from './Mensaje';

const NuevoPresupuesto = ({presupuesto,setPresupuesto,isValidPresupuesto,setIsValidPresupuesto}) => {
  const [mensaje,setMensaje] =useState('');
const handlePresupuesto = (e)=>{
e.preventDefault();
if(!Number(presupuesto) || Number(presupuesto) < 0){
  setMensaje('no es un numero valido')
  return
}
setMensaje('')
console.log("Si es un numero");
setIsValidPresupuesto(true)


}

  return (
    <div className="contenedor-presupuesto contenedor sombra">
    <form action="" className='formulario' onSubmit={handlePresupuesto}>
    <div className="campo">
    <label htmlFor="">Definir Presupuesto</label>
    <input type="number"  value={presupuesto}  className='nuevo-presupuesto' placeholder='añade tu presupuesto' onChange={e=> setPresupuesto(Number(e.target.value))} />

    </div>

    <input type="submit" value="Anadir" />

    {mensaje && <Mensaje tipo="error">{mensaje}</Mensaje>}
</form>        </div>
  )
}

export default NuevoPresupuesto