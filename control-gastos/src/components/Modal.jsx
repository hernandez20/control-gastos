import React, { useEffect, useState } from 'react'
import CerrarBtn from '../assets/img/cerrar.svg'
import Mensaje from './Mensaje'
const Modal = ({
    setModal,
    animarModal,
    setAnimarModal,
    guardarGasto,
    gastoEditar,
    setGastoEditar
}) => {
 
    const [mensaje,setMensaje] =useState("")
    const [nombre,setNombre]=useState("")
    const [cantidad,setCantidad]=useState("")
    const [categoria,setCategoria]=useState("")
    const [fecha, setFecha] = useState('')
    const [id, setId] = useState('')

    
useEffect(()=>{
 if (Object.keys(gastoEditar).length >0){
    setNombre(gastoEditar.nombre)
    setCantidad(gastoEditar.cantidad)
    setCategoria(gastoEditar.categoria)
    setId(gastoEditar.id)
    setFecha(gastoEditar.fecha)
 }
},[])
const handleSubmit = e =>{
e.preventDefault();
if([nombre,cantidad,categoria].includes("")){
setMensaje("todos los campos son obligatorios")

setTimeout(() =>{
setMensaje("")
},1000)
    return
}

guardarGasto({nombre,cantidad,categoria,fecha})
}



    
    const ocultarModal =()=>{
        console.log('Ocultar Modal')
        setAnimarModal(false)
        setGastoEditar({})



        setTimeout(() => {
            setModal(false)

        }, 500);
    }
  return (
    <div className='modal'>
  

        <div className='cerrar-modal'>
        <p>Desde Modal</p>


<img src={CerrarBtn} alt="cerrar modal"
onClick={ocultarModal}/>
       
    </div>



    <form 
    onSubmit={handleSubmit}
    className={`formulario ${animarModal ? "animar":"cerrar"}`} >
        <legend>{gastoEditar.nombre? "Editar Gasto":"Nuevo Gasto"}</legend>

        {mensaje && <Mensaje tipo="error">{mensaje}</Mensaje> }
        <div className='campo'>
            <label htmlFor="nombre">Nombre Gasto</label>
            <input type="text" 
            id='nombre'
            placeholder='añade el nombre del gasto'
            value={nombre}
            onChange={e=>setNombre(e.target.value)}
             />
        </div>


        <div className='campo'>
            <label htmlFor="cantidad">Nombre Gasto</label>
            <input type="number" 
            id='cantidad'
            placeholder='añade la cantidad del gasto'
            value={cantidad}
            onChange={e=>setCantidad(Number(e.target.value))}
             />
        </div>

        <div className='campo'>
            <label htmlFor="categoria">Nombre Gasto</label>
            <select 
            id='categoria'


            value={categoria}
            onChange={e=>setCategoria(e.target.value)}
             >

                <option value="">--seleccione --</option>
                <option value="ahorro">Ahorro</option>
                <option value="comida"> Comida</option>
                <option value="casa">Casa</option>
                <option value="gastos">Gastos</option>
                <option value="ocio">Ocio</option>
                <option value="salud">Salud</option>
                <option value="suscripciones">Suscripciones</option>

             </select>
        </div>
<input type="submit" value={gastoEditar.nombre? "editar Gasto":"Añadir Gasto"} />
        
    </form>
    </div>
  )
}

export default Modal