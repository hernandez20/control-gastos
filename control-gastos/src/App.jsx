import { useState,useEffect } from 'react'
import IconoNuevoGasto from './assets/img/nuevo-gasto.svg'
import  {generarId} from './helpers'

import Filtros from './components/Filtros'
import ListadoGastos from './components/ListadoGastos'
// import reactLogo from './assets/react.svg'
// import viteLogo from '/vite.svg'
import './index.css'
import Header from './components/Header'
import Modal from './components/Modal'
function App() {
  const [presupuesto,setPresupuesto]=useState(
    localStorage.getItem("presupuesto")?? 0
  )
const [isValidPresupuesto,setIsValidPresupuesto] =useState(false)
const [modal,setModal] = useState(false)
const [gastos, setGastos] = useState(
  localStorage.getItem('gastos') ? JSON.parse(localStorage.getItem('gastos')) : []
)
let [gastoEditar, setGastoEditar] = useState({})
const [filtro, setFiltro] = useState('')
const [gastosFiltrados, setGastosFiltrados] = useState([])

const [animarModal,setAnimarModal]= useState(false)
useEffect(() => {
  if(filtro) {
      const gastosFiltrados = gastos.filter( gasto => gasto.categoria === filtro)
      setGastosFiltrados(gastosFiltrados)
  }
}, [filtro]);

useEffect(() => {
 
if(Object.keys(gastoEditar).length > 0){
  handleNuevoGasto()
}      

}, [gastoEditar])
useEffect(() => {
  localStorage.setItem('presupuesto', presupuesto ?? 0)
}, [presupuesto])
useEffect(() => {
  localStorage.setItem('gastos', JSON.stringify(gastos) ?? [])
}, [gastos])
useEffect(() => {
  const presupuestoLS = Number(localStorage.getItem('presupuesto')) ?? 0;

  if(presupuestoLS > 0 ) {
    setIsValidPresupuesto(true)
  }
}, []);

const handleNuevoGasto = () =>{
  setModal(true)

  setTimeout(() => {
setAnimarModal(true)  }, 500);
}

const guardarGasto = gasto => {
  if (gasto.id){
const gastosActualizados =gastos.map(gastoState => gastoState.id === gasto.id? gasto:gastoState)
setGastos(gastosActualizados)
setGastoEditar=({})
  }


  else{
    gasto.id =generarId()
    gasto.fecha = Date.now();
  setGastos([...gastos,gasto])
  }
  





setAnimarModal(false)
setTimeout(()=>{
  setModal(false)
},500)

}

const eliminarGasto = id => {
  const gastosActualizados = gastos.filter( gasto => gasto.id !== id);
  setGastos(gastosActualizados);
}
  return (
    <>
<div className={modal ? "fijar":""}>
  <Header 
   setGastos={setGastos}
  presupuesto={presupuesto}
 setPresupuesto={setPresupuesto}
 isValidPresupuesto={isValidPresupuesto}
 setIsValidPresupuesto={setIsValidPresupuesto}
 gastos={gastos} />



{ isValidPresupuesto && (
  <>
  <main>

<Filtros

filtro={filtro}
setFiltro={setFiltro}
/>

 < ListadoGastos 
 gastos ={gastos}
 filtro={filtro}

 setGastoEditar={setGastoEditar}
 eliminarGasto={eliminarGasto}
 gastosFiltrados={gastosFiltrados}


 />
  </main>
  <div className='nuevo-gasto'>
    <img src={IconoNuevoGasto} alt="icono Nuevo Gasto" onClick={handleNuevoGasto} />
  </div>
  </>

)}

{modal && <Modal setModal={setModal} animarModal={animarModal} 
guardarGasto={guardarGasto} setAnimarModal={setAnimarModal} gastoEditar={gastoEditar}
setGastoEditar={setGastoEditar} ></Modal>}



</div>
    </>
  )
}

export default App
